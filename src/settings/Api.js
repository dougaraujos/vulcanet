const API = {
	url: '/api/',
	headers: {
		'Content-Type': 'application/json'
	}
}

export default API;