/**
 * Component States
 */


const INIT = 'INIT';
const LOADING = 'LOADING';
const SUCCESS = 'SUCCESS';
const FAILURE = 'FAILURE';
const EMPTY = 'EMPTY';

export default {
	INIT,
	LOADING,
	SUCCESS,
	FAILURE,
	EMPTY
}