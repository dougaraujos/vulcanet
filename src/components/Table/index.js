/**
 * Module Dependencies
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import findIndex from 'lodash/findIndex';

import ReactTable from "react-table";
import "react-table/react-table.css";

// Stylesheets
import styles from './styles.scss';

// Icons
import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import { faExclamationCircle } from '@fortawesome/fontawesome-free-solid';


class Table extends Component{
	static propTypes = {
		columns: PropTypes.array,
		data: PropTypes.array,
		loading: PropTypes.bool,
		pivotBy: PropTypes.array,
	};

	static defaultProps = {
		columns: [
			{
				accessor: "title",
				aggregate: (values) => values[0],
				Pivot: ({value, subRows}) => (
					<div>
						{value}
						<span className="counter">{subRows.length}</span>
					</div>
				),
				Header: "Title",
				minWidth: 150,
			},
			{
				accessor: "severity",
				aggregate: (values) => values[0],
				Pivot: ({value, subRows}) => (
					<div>
						<span className={`square is-${value}`} title={`Severity: ${value}`}></span>
						<span className="counter">{subRows.length}</span>
					</div>
				),
				Aggregated: ({value}) => (
					<span className={`square is-${value}`} title={`Severity: ${value}`}></span>
				),
				className: 'text-center',
				Cell: ({value}) => (
					<span className={`square is-${value}`} title={`Severity: ${value}`}></span>
				),
				Header: <div className="icon" title="Severity"><FontAwesomeIcon icon={faExclamationCircle} /></div>,
				id: "severity",
				sortMethod: (a, b, desc) => {
					const severity = ['low', 'medium', 'high', 'critical'];
					const indexA = severity.indexOf(a);
					const indexB = severity.indexOf(b);

					return (indexA > indexB) ? 1 : -1;
				},
				width: 80
			},
			{
				accessor: "status",
				aggregate: (values) => values[0],
				Pivot: ({value, subRows}) => (
					<div>
						<span className="label">{value}</span>
						<span className="counter">{subRows.length}</span>
					</div>
				),
				className: 'text-center',
				Cell: ({value}) => (
					<span className="label">{value}</span>
				),
				Header: <div className="text-center">Status</div>,
				width: 110
			},
			{
				accessor: "trader",
				aggregate: (values) => values[0],
				Pivot: ({value, subRows}) => (
					<div>
						{value}
						<span className="counter">{subRows.length}</span>
					</div>
				),
				Header: "Trader"
			},
			{
				pivot: true,
				Header: () => null
			},
			{
				aggregate: (values) => values[0],
				Pivot: ({value, subRows}) => (
					<div>
						{value}
						<span className="counter">{subRows.length}</span>
					</div>
				),
				Header: "Counterparty",
				accessor: "counterparty"
			},
			{
				aggregate: (values) => values[0],
				Pivot: ({value, subRows}) => (
					<div>
						{value}
						<span className="counter">{subRows.length}</span>
					</div>
				),
				accessor: "book",
				Header: "Book",
				width: 100
			},
			{
				aggregate: (values) => values[0],
				Pivot: ({value, subRows}) => (
					<div>
						{value}
						<span className="counter">{subRows.length}</span>
					</div>
				),
				accessor: "source",
				Header: "Source",
				width: 120
			}
		],
		data: [],
		loading: true
	};

	constructor(props){
		super(props)

		this.state = {
			columns: props.columns
		}
	}

	componentWillReceiveProps(nextProps){
		const pivotBy = nextProps.pivotBy[0];

		if(pivotBy !== this.props.pivotBy[0]){
			let { columns } = this.state;
			let pivotIndex = findIndex(columns, {pivot: true});

			if(pivotIndex > -1){
				columns.splice(pivotIndex, 1);

				let newPivotIndex = findIndex(columns, {accessor: pivotBy});
				
				columns.splice(newPivotIndex, 0, {
					pivot: true,
					Header: () => null
				});

				this.setState({
					columns
				})
			}

		}
	}

	render(){
		const { columns, data, filtered, loading, pivotBy, ...options } = this.props;

		const defaultOptions = {
			loading,
			filtered,
			pivotBy,
			className: 'table',
			collapseOnSortingChange: false,
			filterable: true,
			filterAll: true,
			minRows: 0,
			defaultSorted: [{
				id: 'severity',
				desc: true
			}],
			previousText: 'Anterior',
			nextText: 'Próxima',
			loadingText: 'Carregando...',
			noDataText: 'Nenhum item encontrado',
			pageText: 'Página',
			ofText: 'de',
			rowsText: 'itens',
			defaultFilterMethod: (filter, row, column) => {
				const id = filter.pivotId || filter.id;
				const value = String(row[id]).toLowerCase();

				const find = filter.value.indexOf(value) > -1
				return row[id] !== undefined && filter.value.length ? find : true
			}
		}

		return(
			<ReactTable data={data} {...defaultOptions} columns={columns} />
		)
	}
}

export default Table;