/**
 * Module Dependencies
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';

// Stylesheets
import styles from './styles.scss';

// Icons
import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import { faSearch } from '@fortawesome/fontawesome-free-solid';


class Search extends Component {
	static propTypes = {
		counter: PropTypes.number,
	};

	static defaultProps = {
		counter: 0
	};

	render(){
		const { counter } = this.props;

		return (
			<div className={styles.search}>
				<FontAwesomeIcon icon={faSearch} className={styles.icon} />

				<input type="text" className={styles.input} placeholder="Search for Alerts..." />

				<span className={styles.label}>{counter}</span>
			</div>
		)
	}
}


export default Search;