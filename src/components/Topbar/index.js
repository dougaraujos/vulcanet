/**
 * Module Dependencies
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

// Components
import Search from 'components/Search';

//Selectors
import { itemSelectors } from 'state/ducks/item';

// Stylesheets
import styles from './styles.scss';

// Icons
import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import { faBars, faThLarge } from '@fortawesome/fontawesome-free-solid';


class Topbar extends Component {
	static propTypes = {
		pivotBy: PropTypes.string,
	};

	renderFilters(filters){
		const { changeGroupBy, pivotBy } = this.props;

		return(
			<select className={styles.select} onChange={(e) => changeGroupBy(e)}>
				{filters.map(({name, endpoint}) => 
					<option key={endpoint} value={endpoint} selected={pivotBy == endpoint}>{name}</option>
				)}
			</select>
		)
	}

	render(){
		const { filters, items } = this.props;

		return (
			<header className={styles.topbar}>
				<Search counter={items.length} />

				<div className={styles.items}>
					<div className={`${styles.item} ${styles.isActive}`}>
						<FontAwesomeIcon icon={faBars} className={styles.icon} />
						List
					</div>

					<div className={styles.item}>
						<FontAwesomeIcon icon={faThLarge} className={styles.icon} />
						Group by {this.renderFilters(filters)}
					</div>

					<div className={`${styles.item} ${styles.isAction}`}>
						Take Action
					</div>
				</div>
			</header>
		)
	}
}


const mapStateToProps = (state) => {
	const { filter, item } = state;
	const { filters } = filter;
	const { items } = item;

	return {
		filters,
		items
	}
};

export default connect(mapStateToProps)(Topbar);