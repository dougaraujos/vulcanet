/**
 * Module Dependencies
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import Collapse from 'rc-collapse';

//Actions
import { filterActions } from 'state/ducks/filter';

// Stylesheets
import styles from './styles.scss';

// Images
import logo from 'assets/images/logo-radar.png';

// Icons
import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import { faUserCircle, faBell, faQuestionCircle, faAngleLeft, faAngleRight, faFilter } from '@fortawesome/fontawesome-free-solid';


class Sidebar extends Component {
	constructor(props){
		super(props)

		this.state = {
			menuClosed: true
		}
	}

	componentWillMount() {
		const { fetchFilters } = this.props;

		fetchFilters();
	}

	componentWillReceiveProps(newProps){
		const { filters, fetch } = newProps;

		if(!this.props.filters.length){
			filters.map(({endpoint}) => fetch(endpoint))
		}
	}

	toggleMenu(){
		this.setState({
			menuClosed: !this.state.menuClosed
		})
	}

	renderFilters(){
		const { filters } = this.props;
		const { Panel } = Collapse;

		return (
			<Collapse defaultActiveKey="open-flags" accordion={true}>
				<Panel key="open-flags" showArrow={false} header={
					<div className={styles.sets}>
						<FontAwesomeIcon icon={faAngleLeft} className={styles.arrow}/>
						Sets

						<span className={styles.openFlags}>Open Flags</span>
					</div>
				}>
					<div className={styles.find}>
						<FontAwesomeIcon icon={faFilter} />
						<input type="text" className={styles.input} placeholder="Find Properties..." />
					</div>
				</Panel>
				{filters.map(({id, name, items}) => (
					<Panel key={`${name}-${id}`} header={<div>{name} <FontAwesomeIcon icon={faAngleRight} className="arrow" /></div>} showArrow={false}>
						{items && this.renderFilterItems(items, name)}
					</Panel>
				))}
			</Collapse>
		)
	}

	renderFilterItems(items, id){
		const { changeFilter } = this.props;
		id = id.toLowerCase();

		const handleClick = (e, value) => {
			changeFilter(id, value);
			e.target.classList.toggle(styles.isActive);
		}

		return items.map(({id, name}) => {
			const value = name.toLowerCase()
		
			return (
				<span key={`${name}-${id}`} className={styles.label} onClick={(e) => handleClick(e, value)}>
					{name}
				</span>
			)
		})
	}

	render(){
		const { menuClosed } = this.state;

		return (
			<aside className={`${styles.sidebar} ${menuClosed && styles.isClosed}`}>
				<div className={styles.menu} onClick={::this.toggleMenu}>
					Menu
					<FontAwesomeIcon icon={faAngleLeft} className={styles.icon} />
				</div>
				<header className={styles.header}>
					<Link to="/">
						<img src={logo} alt="Logo Radar"/>
					</Link>

					<div>
						<FontAwesomeIcon icon={faUserCircle} className={styles.icon} />
						<FontAwesomeIcon icon={faBell} className={styles.icon} />
						<FontAwesomeIcon icon={faQuestionCircle} className={styles.icon} />
					</div>
				</header>

				{this.renderFilters()}
			</aside>
		)
	}
}


const mapStateToProps = (state) => {
	const { filter } = state;
	const { filters, state: filterState } = filter;

	return {
		filters,
		filterState
	}
};

const mapDispatchToProps = (dispatch) => ({
	fetchFilters: () => dispatch(filterActions.fetchAll()),
	fetch: (filter) => dispatch(filterActions.fetch(filter))
});

export default connect(mapStateToProps, mapDispatchToProps)(Sidebar);