/**
 * Filter
 */


import reducer from './reducers';

export { default as filterActions } from './actions';

export default reducer;
