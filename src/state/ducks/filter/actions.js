/**
 * Filter Actions
 */


import types from './types';


const fetchAll = () => ({
	type: types.FETCH_ALL,
	meta: {
		async: true,
		endpoint: 'filter/all.json'
	}
});

const fetch = (filter) => ({
	type: types.FETCH,
	meta: {
		async: true,
		endpoint: `${filter}/all.json`,
		name: filter
	}
});


export default {
	fetchAll,
	fetch
}