/**
 * Filter Reducers
 */


import types from './types';
import STATE from 'settings/State';

// Utils
import { createReducer } from 'state/utils';
import objectAssign from 'object-assign';


const initialState = {
	filters: [],
	state: STATE.INIT
};

const filterReducer = createReducer(initialState)({
	[types.FETCH_ALL]: (state, action) => {
		return objectAssign({}, state, {
			state: STATE.LOADING
		})
	},

	[types.FETCH_ALL_SUCCESS]: (state, action) => {
		const filters = action.payload;
		const filterState = filters.length ? STATE.SUCCESS : STATE.EMPTY;

		return objectAssign({}, state, {
			filters,
			state: filterState
		})
	},

	[types.FETCH_ALL_FAILURE]: (state, action) => {
		return objectAssign({}, state, initialState, {
			error: action.error,
			state: STATE.FAILURE,
		})
	},

	[types.FETCH]: (state, action) => {
		return objectAssign({}, state, {
			state: STATE.LOADING
		})
	},

	[types.FETCH_SUCCESS]: (state, action) => {
		const { filters } = state;

		const { name } = action.meta;
		const filterState = filters.length ? STATE.SUCCESS : STATE.EMPTY;


		// Adicionar itens de cada filtro
		let current;
		let newFilters = [...filters];

		filters.map((filter, index) => {
			if(Object.values(filter).indexOf(name) > -1){
				current = index;
			}
		});

		newFilters[current].items = action.payload;

		return objectAssign({}, state, {
			filters: newFilters,
			state: filterState
		})
	},

	[types.FETCH_FAILURE]: (state, action) => {
		return objectAssign({}, state, initialState, {
			error: action.error,
			state: STATE.FAILURE,
		})
	}
});

export default filterReducer;
