/**
 * Filter Types
 */


const FETCH_ALL = 'filter/FETCH_ALL';
const FETCH_ALL_SUCCESS = 'filter/FETCH_ALL_SUCCESS';
const FETCH_ALL_FAILURE = 'filter/FETCH_ALL_FAILURE';

const FETCH = 'filter/FETCH';
const FETCH_SUCCESS = 'filter/FETCH_SUCCESS';
const FETCH_FAILURE = 'filter/FETCH_FAILURE';


export default {
	FETCH_ALL,
	FETCH_ALL_SUCCESS,
	FETCH_ALL_FAILURE,
	FETCH,
	FETCH_SUCCESS,
	FETCH_FAILURE
};
