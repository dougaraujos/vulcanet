/**
 * Item Reducers
 */


import types from './types';
import STATE from 'settings/State';

// Utils
import { createReducer } from 'state/utils';
import objectAssign from 'object-assign';


const initialState = {
	items: [],
	state: STATE.INIT
};

const itemReducer = createReducer(initialState)({
	[types.FETCH_ALL]: (state, action) => {
		return objectAssign({}, state, {
			state: STATE.LOADING
		})
	},

	[types.FETCH_ALL_SUCCESS]: (state, action) => {
		const items = action.payload;
		const itemState = items.length ? STATE.SUCCESS : STATE.EMPTY;

		return objectAssign({}, state, {
			items,
			state: itemState
		})
	},

	[types.FETCH_ALL_FAILURE]: (state, action) => {
		return objectAssign({}, state, initialState, {
			error: action.error,
			state: STATE.FAILURE,
		})
	}
});

export default itemReducer;
