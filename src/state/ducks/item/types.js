/**
 * Item Types
 */


const FETCH_ALL = 'item/FETCH_ALL';
const FETCH_ALL_SUCCESS = 'item/FETCH_ALL_SUCCESS';
const FETCH_ALL_FAILURE = 'item/FETCH_ALL_FAILURE';


export default {
	FETCH_ALL,
	FETCH_ALL_SUCCESS,
	FETCH_ALL_FAILURE
};
