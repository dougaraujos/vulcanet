/**
 * Item Actions
 */


import types from './types';


const fetchAll = () => ({
	type: types.FETCH_ALL,
	meta: {
		async: true,
		endpoint: 'item/all.json'
	}
});


export default {
	fetchAll
}