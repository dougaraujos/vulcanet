/**
 * Item Selectors
 */


const getItems = (items) => {
	return items.map((item) => ({
		title: item.title,
		severity: item.severity.name,
		status: item.status.name,
		trader: item.trader.name,
		counterparty: item.counterparty.name,
		book: item.book,
		source: item.source.name
	}))
}


export default {
	getItems
};
