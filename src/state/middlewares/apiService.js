/**
 * Module Dependencies
 */

import 'whatwg-fetch';
import API from 'settings/Api';


export default (store) => (next) => (action) => {
	const result = next(action);

	if(!action.meta || !action.meta.async){
		return result;
	}

	const { endpoint, method = "GET", body, headers, baseUrl } = action.meta;

	if(!endpoint){
		throw new Error(`'endpoint' not specified for async action ${action.type}`);
	}

	const handleFetch = () => {
		const url = `${API.url}${endpoint}`;

		return fetch(url).then(
			response => handleResponse(response, action, next),
			error => handleErrors(error, action, next),
		);
	}

	return handleFetch();
};

const handleResponse = (response, action, next) => {
	return response.text().then(data => {
		if(response.status === 200){
			let payload;

			if(response.headers.get('content-type')){
				try{
					payload = JSON.parse(data);
				} catch(e){
					handleErrors({}, action, next)
				}
			}

			next({
				type: `${action.type}_SUCCESS`,
				payload: payload,
				meta: action.meta,
			});

			return payload;
		}

		return handleErrors(data, action, next);
	});
}

const handleErrors = (error, action, next) => {
	next({
		type: `${action.type}_FAILURE`,
		error,
		meta: action.meta,
	})
}
