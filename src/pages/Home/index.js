/**
 * Module Dependencies
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import findIndex from 'lodash/findIndex';

import STATE from 'settings/State';

//Actions
import { itemActions } from 'state/ducks/item';

//Selectors
import { itemSelectors } from 'state/ducks/item';

//Components
import Sidebar from 'components/Sidebar';
import Table from 'components/Table';
import Topbar from 'components/Topbar';

// Stylesheets
import styles from './styles.scss';


class Home extends Component{
	constructor(props) {
		super(props);

		this.state = {
			filtered: [],
			pivotBy: ['trader']
		}
	}

	componentWillMount() {
		const { fetchItems } = this.props;

		fetchItems();
	}

	changeFilter(id, value){
		const { filtered } = this.state;
		let newFiltered = [...filtered];
		let indexId;
		let indexValue;

		indexId = findIndex(filtered, ['id', id]);

		if(indexId > -1){
			let values = [...newFiltered[indexId].value];

			indexValue = values.indexOf(value);

			if(indexValue > -1){
				newFiltered[indexId].value.splice(indexValue, 1);
			} else {
				newFiltered[indexId].value.push(value);
			}

		} else {
			newFiltered.push({id, value: [value]})
		}

		this.setState({
			filtered: newFiltered
		})
	}

	changeGroupBy(e){
		this.setState({
			pivotBy: [...e.target.value]
		})
	}

	render(){
		const { items, itemState } = this.props;
		const { filtered, pivotBy } = this.state;
		const loading = itemState == STATE.LOADING;

		return(
			<div className={styles.content}>
				<Topbar changeGroupBy={::this.changeGroupBy} pivotBy={pivotBy[0]}/>

				<Sidebar changeFilter={::this.changeFilter} />

				<section>
					<Table data={items} filtered={filtered} loading={loading} pivotBy={pivotBy} />
				</section>
			</div>
		)
	}
}


const mapStateToProps = (state) => {
	const { item } = state;
	const { items, state: itemState } = item;

	return {
		items: itemSelectors.getItems(items),
		itemState
	}
};

const mapDispatchToProps = (dispatch) => ({
	fetchItems: () => dispatch(itemActions.fetchAll()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);