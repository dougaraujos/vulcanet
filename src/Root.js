/**
 * Module Dependencies
 */

import React, { Component } from 'react';
import { applyMiddleware, combineReducers, compose, createStore } from 'redux';
import { Provider } from 'react-redux';
import { ConnectedRouter, routerMiddleware, routerReducer } from 'react-router-redux';
import { Route, Router, Switch } from 'react-router-dom';

//Middlewares
import thunk from 'redux-thunk';
import { apiService } from 'state/middlewares';
import { createBrowserHistory } from 'history';

//Reducers
import * as reducers from 'state/ducks';

//Routes
import routes from 'routes';

//Styles
import stylesheets from 'stylesheets';

export default class Root extends Component {
	constructor(props) {
		super(props);

		this.history = createBrowserHistory();

		const reducer = combineReducers(reducers);
		const middlewares = applyMiddleware(routerMiddleware(this.history), apiService, thunk);
		const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

		this.store = createStore(reducer, undefined, composeEnhancers(middlewares));
	}

	render() {
		return (
			<Provider store={this.store}>
				<ConnectedRouter history={this.history}>
					<Switch>
						{routes.map(route => <Route key={route.path} {...route} component={() => route.component} />)}
					</Switch>
				</ConnectedRouter>
			</Provider>
		)
	}
}